module gitlab.com/skyhuborg/tracker

go 1.13

require (
	cloud.google.com/go v0.72.0 // indirect
	cloud.google.com/go/storage v1.12.0
	github.com/Microsoft/go-winio v0.4.15 // indirect
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/ahmetalpbalkan/dlog v0.0.0-20170105205344-4fb5f8204f26
	github.com/denisenkom/go-mssqldb v0.9.0 // indirect
	github.com/desertbit/timer v0.0.0-20180107155436-c41aec40b27f // indirect
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v1.13.1
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/golang/protobuf v1.4.3
	github.com/google/go-cmp v0.5.3 // indirect
	github.com/google/uuid v1.1.2
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/improbable-eng/grpc-web v0.13.0
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/now v1.1.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/lib/pq v1.8.0 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/mwitkow/go-conntrack v0.0.0-20190716064945-2f068394615f // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/rs/cors v1.7.0 // indirect
	github.com/stratoberry/go-gpsd v0.0.0-20161204231141-54ddcfa61f47
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/xfrr/goffmpeg v0.0.0-20200825100927-5550d238df5c
	go.bug.st/serial v1.1.1
	go.opencensus.io v0.22.5 // indirect
	gocv.io/x/gocv v0.25.0
	golang.org/x/crypto v0.0.0-20201116153603-4be66e5b6582
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b // indirect
	golang.org/x/oauth2 v0.0.0-20201109201403-9fd604954f58 // indirect
	golang.org/x/sys v0.0.0-20201116194326-cc9327a14d48 // indirect
	golang.org/x/tools v0.0.0-20201117021029-3c3a81204b10 // indirect
	google.golang.org/api v0.35.0
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/genproto v0.0.0-20201116205149-79184cff4dfe // indirect
	google.golang.org/grpc v1.33.2
	google.golang.org/protobuf v1.25.0
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
	gopkg.in/yaml.v2 v2.3.0
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)

replace gocv.io/x/gocv => github.com/nibbleshift/gocv v0.23.1-0.20200808203906-c5cee6148248
