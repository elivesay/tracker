/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package trackerd

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"

	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/skyhuborg/tracker/internal/db"
	pb "gitlab.com/skyhuborg/tracker/internal/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
)

type Status struct {
	Id    int    `bson:"_id,omitempty"`
	Uuid  string `bson:"uuid"`
	Name  string `bson:"name"`
	Build string `bson:"build"`
}

type Server struct {
	pb.UnimplementedTrackerdServer
	Handle     *grpc.Server
	ListenPort int
	EnableTls  bool
	TlsKey     string
	TlsCert    string
	DbHost     string
	DbPort     int
	DbUser     string
	DbPassword string
	DbName     string

	dbUri string
	db    *db.DB
}

func (s *Server) Start() {
	s.dbUri = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", s.DbUser, s.DbPassword, s.DbHost, s.DbPort, s.DbName)

	s.db = db.NewDB()

	err := s.db.Connect(s.dbUri)

	if err != nil {
		log.Fatalf("DB Connect failed: %s\n", err)
		return
	}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", s.ListenPort))

	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s.Handle = grpc.NewServer()

	pb.RegisterTrackerdServer(s.Handle, s)

	grpclog.SetLogger(log.New(os.Stdout, "trackerd: ", log.LstdFlags))

	grpclog.Printf("Starting GRPC on port %d", s.ListenPort)

	if err := s.Handle.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}

/*
CREATE TABLE sensor (
  id int auto_increment primary key,
  tracker_id VARCHAR(36) NOT NULL,
  event_id VARCHAR(36) NOT NULL,
  gps_time datetime,
  insert_tm datetime DEFAULT CURRENT_TIMESTAMP,
  report VARCHAR(36) NOT NULL
)
*/

func (s *Server) AddSensor(ctx context.Context, in *pb.SensorReport) (*pb.SensorReportResponse, error) {
	s.db.Save(in)
	log.Printf("Sensor: [%s] [%s] [%s] [%s] [%f,%f] [%s]\n",
		in.Tracker.Uuid,
		in.Tracker.Name,
		in.Tracker.BuildId,
		in.Tracker.BuildDate,
		in.Tracker.Latitude,
		in.Tracker.Longitude,
		in.Tracker.Time.AsTime().String())
	return &pb.SensorReportResponse{}, nil
}

func (s *Server) AddEvent(ctx context.Context, in *pb.Event) (*pb.EventResponse, error) {
	log.Printf("Event: [%s] [%s] Event=%s [f,%f] [%s]\n",
		in.Tracker.Uuid,
		in.Tracker.Name,
		in.Uuid,
		in.Tracker.Latitude,
		in.Tracker.Longitude,
		in.Tracker.Time.AsTime().String())
	s.db.Save(in)
	return &pb.EventResponse{}, nil
}

func (s *Server) AddVideoEvent(ctx context.Context, in *pb.VideoEvent) (*pb.VideoEventResponse, error) {
	log.Printf("VideoEvent: [%s] [%s] [%s] [%s] [%f,%f] [%s]\n",
		in.Tracker.Uuid,
		in.Tracker.Name,
		in.EventUuid,
		in.Uri,
		in.Tracker.Latitude,
		in.Tracker.Longitude,
		in.Tracker.Time.AsTime().String())
	s.db.Save(in)
	return &pb.VideoEventResponse{}, nil
}

func (s *Server) Register(ctx context.Context, in *pb.TrackerInfo) (*pb.RegisterResponse, error) {
	s.db.Save(in)
	return &pb.RegisterResponse{}, nil
}
