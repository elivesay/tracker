/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

package event

import (
	"github.com/golang/protobuf/ptypes"
	"github.com/google/uuid"
	//pb "gitlab.com/skyhuborg/proto-trackerd-go"
	"gitlab.com/skyhuborg/tracker/internal/common"
	pb "gitlab.com/skyhuborg/tracker/internal/proto"
	"log"
	"sync"
	"time"
)

type Event struct {
	Uuid       string    // Id of the event, randomly generated UUID hash
	inProgress bool      // true if the event is in progress
	isComplete bool      // true if the event is in progress
	StartTime  time.Time // time the event started
	EndTime    time.Time // time the event ended
	Duration   int64     // how long the event lasted in ms
	//Type       pb.EventType // The type of event, see EventType
	Source   Source // Source is where the event originated from
	wg       sync.WaitGroup
	refCount int
}

func NewEvent() *Event {
	e := Event{}
	e.inProgress = false
	e.isComplete = true
	return &e
}

func (e *Event) ToPb() *pb.Event {
	start, _ := ptypes.TimestampProto(e.StartTime)
	end, _ := ptypes.TimestampProto(e.EndTime)
	gps_time, _ := ptypes.TimestampProto(time.Now())

	info := &pb.TrackerInfo{
		Uuid:      common.Info.TrackerId,
		Name:      common.Info.TrackerName,
		Time:      gps_time,
		Longitude: common.Info.Longitude,
		Latitude:  common.Info.Latitude,
	}
	pbEvent := &pb.Event{
		Tracker:   info,
		Uuid:      e.Uuid,
		Source:    &e.Source.EventSource,
		StartTime: start,
		EndTime:   end,
		Duration:  e.Duration,
	}
	return pbEvent
}

func (e *Event) SetWaitGroup(refCount int) {
	e.refCount = refCount
}

func (e *Event) Start() {
	if e.inProgress == true {
		return
	}

	e.wg.Add(e.refCount)

	e.isComplete = false

	e.StartTime = time.Now()

	id, err := uuid.NewRandom()

	if err != nil {
		log.Printf("Error generating random uuid: %s\n", err)
		return
	}
	e.Uuid = id.String()

	e.inProgress = true
}

func (e *Event) Stop() {
	if e.inProgress == false {
		return
	}

	e.inProgress = false

	e.EndTime = time.Now()

	// wait for callers with references to finish
	e.wg.Wait()

	elapsed := e.EndTime.Sub(e.StartTime)

	e.Duration = elapsed.Milliseconds()
	e.isComplete = true
}

/* setters */
func (e *Event) SetSource(Source *Source) {
	e.Source = *Source
}

/* setters */
func (e *Event) GetId() string {
	return e.Uuid
}

func (e *Event) GetInProgress() bool {
	return e.inProgress
}

func (e *Event) GetIsComplete() bool {
	return e.isComplete
}

func (e *Event) GetStartTime() time.Time {
	return e.StartTime
}

func (e *Event) GetEndTime() time.Time {
	return e.EndTime
}

func (e *Event) GetDuration() int64 {
	return e.Duration
}

func (e *Event) GetSourceType() pb.EventType {
	return e.Source.Type
}

func (e *Event) GetSource() *Source {
	return &e.Source
}

// callers call this to indicate they are finished with the event
func (e *Event) Done() {
	e.wg.Done()
}
