/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package db

import (
	"log"
	"sync"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type Event struct {
	Id        string    `gorm:"type:varchar(40); PRIMARY_KEY"`
	CreatedAt time.Time `gorm:"type:datetime;Column:created_at"`
	StartedAt time.Time `gorm:"type:datetime;Column:started_at"`
	EndedAt   time.Time `gorm:"type:datetime;Column:ended_at"`
	Duration  int64     `gorm:"type:decimal"`
	Type      string    `gorm:"type:varchar(32)"`
	Source    string    `gorm:"type:varchar(32)"`
	Sensor    string    `gorm:"type:varchar(32)"`
}

type VideoEvent struct {
	EventId         string    `gorm:"column:event_id; PRIMARY_KEY" json:"event_id"`
	CreatedAt       time.Time `gorm:"type:datetime;Column:created_at"`
	Uri             string    `gorm:"type:varchar(512)"`
	WebUri          string    `gorm:"type:varchar(512);default:NULL"`
	Thumbnail       string    `gorm:"type:varchar(256)"`
	IsUploaded      bool      `gorm:"default:false"`
	IsPending       bool      `gorm:"default:false"`
	IsInprogress    bool      `gorm:"default:false"`
	TranscodeFailed bool      `gorm:"default:false"`
}

type Sensor struct {
	EventId   string    `gorm:"column:event_id;null;index;default:NULL" json:"event_id"`
	CreatedAt time.Time `gorm:"type:datetime;Column:created_at"`
	Data      []byte    `gorm:"type:blob"`
}

type DB struct {
	H     *gorm.DB
	mutex sync.Mutex
}

func (db *DB) Open(driver string, uri string) (err error) {
	var nAttempts int = 0
	var maxAttempts int = 180

	for {
		if nAttempts == 1 {
			log.Printf("Waiting for database to come online\n")
		} else if nAttempts > maxAttempts {
			log.Printf("Failed connecting to database after %d seconds\n", maxAttempts)
			return
		}

		db.H, err = gorm.Open(driver, uri)

		if err == nil {
			db.H.DB().SetConnMaxLifetime(24 * time.Hour)
			db.H.DB().SetMaxIdleConns(20)
			db.H.DB().SetMaxOpenConns(20)
			break
		}

		time.Sleep(1 * time.Second)
		nAttempts++
	}
	log.Printf("Successfully connected to %s\n", driver)

	err = db.H.AutoMigrate(
		&Event{},
		&VideoEvent{},
		&Sensor{}).Error

	if err != nil {
		log.Printf("Migrate failed: %s\n", err)
	}

	return
}

func (db *DB) Save(v interface{}) {
	db.H.Save(v)
}

func (db *DB) Close() {
	db.H.Close()
	db = nil
}

func (db *DB) StartEvent(uuid string, ts time.Time, eventType string, eventSource string, sensor string) {
	var (
		rec     Event
		endTime time.Time
	)

	rec.Id = uuid
	rec.StartedAt = ts
	rec.Type = eventType
	rec.Source = eventSource
	rec.Sensor = sensor
	rec.EndedAt = endTime

	err := db.H.Create(&rec).Error

	if err != nil {
		log.Printf("Create returned: %s\n", err)
	}
}

func (db *DB) StopEvent(
	uuid string,
	ts time.Time,
	duration int64) {
	var (
		rec Event
	)

	rec.Id = uuid

	err := db.H.Model(&rec).Updates(Event{EndedAt: ts, Duration: duration}).Error

	if err != nil {
		log.Printf("Update returned: %s\n", err)
	}
}

func (db *DB) AddVideoEvent(eventId string, videoUri string, thumbnailUri string) (err error) {
	var (
		rec VideoEvent
	)

	rec.EventId = eventId
	rec.Uri = videoUri
	rec.Thumbnail = thumbnailUri

	err = db.H.Create(&rec).Error

	if err != nil {
		log.Printf("Failed adding VideoEvent: %s\n", err)
	}
	return
}

func (db *DB) SetVideoEventWebUri(eventId string, webUri string, transcodeFailed bool) {
	rec := VideoEvent{EventId: eventId}
	err := db.H.Model(&rec).Update("web_uri", webUri, "transcode_failed", transcodeFailed).Error

	if err != nil {
		log.Fatalf("%s\n", err)
	}
}

func (db *DB) AddEvent(rec *Event, event_data interface{}) (err error) {
	db.H.Create(rec)
	switch event_data.(type) {
	case *VideoEvent:
		ed, ok := event_data.(*VideoEvent)

		if ok == true {
			ed.EventId = rec.Id
			db.H.Create(event_data)
		}
	default:
		log.Println("unknown")
	}

	return
}

func (db *DB) GetEvents(limit int32) (events []Event, count int32, err error) {
	db.H.Table("events").Count(&count)
	db.H.Order("created_at desc").Limit(limit).Order("created_at desc").Find(&events)

	return
}

func (db *DB) GetVideosForEvent(uuid string) (videoEvents []VideoEvent, err error) {
	db.H.Table("video_events").Where("event_id = ?", uuid).Find(&videoEvents)
	return
}

func (db *DB) GetSensorsForEvent(uuid string) (sensors []Sensor, err error) {
	db.H.Table("sensors").Where("event_id = ?", uuid).Find(&sensors)
	return
}

func (db *DB) GetEvent(uuid string) (event Event, err error) {
	db.H.Table("events").Where("id = ?", uuid).First(&event)
	return
}

func (db *DB) GetSensors(limit int32) (sensors []Sensor, count int32, err error) {
	db.H.Table("sensors").Count(&count)
	db.H.Order("created_at desc").Limit(limit).Find(&sensors)
	return
}

func (db *DB) GetVideoEvents(limit int32, page int32) (events []VideoEvent, count int32, err error) {
	var (
		offset int32
	)

	db.H.Table("video_events").Count(&count)

	offset = 0

	if page > 1 {
		offset = page * limit
	}

	db.H.Order("created_at desc").Limit(limit).Offset(offset).Find(&events)
	return
}

func (db *DB) GetVideoEventNotUploaded() (videoEvent VideoEvent, err error) {
	err = db.H.Where("is_uploaded = ? AND is_pending = ?", false, false).First(&videoEvent).Error
	return
}

func (db *DB) AddSensorData(rec *Sensor) (err error) {
	db.H.Create(rec)
	return nil
}
