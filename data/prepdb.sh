#!/bin/bash

set +e

WORKSPACE=$1/data
mkdir -p $WORKSPACE/mariadb
mkdir -p $WORKSPACE/mysql
mkdir -p $WORKSPACE/bucket

docker network create skyhub

if [ "$( docker container inspect -f '{{.State.Running}}' skyhub-mariadb )" != "true" ]; then
	docker run -d --rm --name skyhub-mariadb --net=skyhub --health-cmd='mysqladmin ping --silent' -v $WORKSPACE/mariadb:/var/lib/mysql -p 3306:3306 --env MYSQL_USER=skyhub --env MYSQL_PASSWORD=3fSS34f03lls --env MYSQL_ROOT_PASSWORD=byMp3nTXpaKeB7Vz --env MYSQL_ROOT_HOST=% mariadb:10.1
fi

if [ "$( docker container inspect -f '{{.State.Running}}' skyhub-mysql )" != "true" ]; then
	docker run -d --rm --name skyhub-mysql --net=skyhub --health-cmd='mysqladmin ping --silent' -v $WORKSPACE/mysql:/var/lib/mysql -p 3307:3306 --env MYSQL_USER=skyhub --env MYSQL_PASSWORD=3fSS34f03lls --env MYSQL_ROOT_PASSWORD=byMp3nTXpaKeB7Vz --env MYSQL_ROOT_HOST=% mysql:latest
fi

if [ "$( docker container inspect -f '{{.State.Running}}' skyhub-gcs )" != "true" ]; then
	docker run -d --rm --name skyhub-gcs -p 4443:4443 -v $WORKSPACE/bucket:/data fsouza/fake-gcs-server
fi

if [ "$( docker container inspect -f '{{.State.Running}}' skyhub-elastic )" != "true" ]; then
	docker run --rm -d --name skyhub-elastic --net=skyhub -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.9.2
fi

if [ "$( docker container inspect -f '{{.State.Running}}' skyhub-kibana )" != "true" ]; then
	docker run --rm -d --name skyhub-kibana --net=skyhub -p 5601:5601 --env ELASTICSEARCH_HOSTS=http://skyhub-elastic:9200 kibana:7.9.2
fi

STATUS=`docker inspect skyhub-mysql --format '{{.State.Health.Status}}' | tr -d '\n'`

while [[ "$STATUS" != "healthy" ]]; do
	sleep 1
	STATUS=`docker inspect skyhub-mysql --format '{{.State.Health.Status}}' | tr -d '\n'`
done

STATUS=`docker inspect skyhub-mariadb --format '{{.State.Health.Status}}' | tr -d '\n'`

while [[ "$STATUS" != "healthy" ]]; do
	sleep 1
	STATUS=`docker inspect skyhub-mariadb --format '{{.State.Health.Status}}' | tr -d '\n'`
done

docker exec skyhub-mysql mysql -u root -pbyMp3nTXpaKeB7Vz -e 'create database skyhub;' 2>&1 >/dev/null
docker exec skyhub-mariadb mysql -u root -pbyMp3nTXpaKeB7Vz -e 'create database skyhub_tracker;' 2>&1 >/dev/null
docker exec skyhub-mysql mysql -u root -pbyMp3nTXpaKeB7Vz -e "GRANT REPLICATION SLAVE ON *.* TO 'skyhub'@'%'; GRANT RELOAD ON *.* TO 'skyhub'@'%'; SET GLOBAL binlog_format = 'ROW'; SET binlog_row_image = 'full';" 2>&1 >/dev/null

curl -X DELETE "localhost:9200/gps_tpv_reports"
curl -X PUT "localhost:9200/gps_tpv_reports" -H 'Content-Type: application/json' -d @./data/kibana/_index_gps_tpv_reports.json

curl -X DELETE "localhost:9200/_scripts/prep_lat_lon"
cat ./data/kibana/_script_prep_lat_lon.json | tr -d '\r' | tr -d '\n' | curl -X POST "localhost:9200/_scripts/prep_lat_lon?pretty" -H 'Content-Type: application/json' -d @-

curl -X DELETE "localhost:9200/_ingest/pipeline/convert_geo"
curl -X PUT "localhost:9200/_ingest/pipeline/convert_geo?pretty" -H 'Content-Type: application/json' -d @./data/kibana/_ingest_pipeline_convert_geo.json

if [ "$( docker container inspect -f '{{.State.Running}}' skyhub-elasticsync )" != "true" ]; then
	docker run --rm -d --name skyhub-elasticsync --net=skyhub -v $WORKSPACE/river.toml:/app/etc/river.toml skyhuborg/elasticsync:master
fi

set -e
exit 0
