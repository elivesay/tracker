/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	//"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"
	"google.golang.org/protobuf/proto"

	//pb "gitlab.com/skyhuborg/proto-trackerd-go"
	"gitlab.com/skyhuborg/tracker/internal/common"
	"gitlab.com/skyhuborg/tracker/internal/event"
	pb "gitlab.com/skyhuborg/tracker/internal/proto"
	"gitlab.com/skyhuborg/tracker/internal/sensor"
	"gitlab.com/skyhuborg/tracker/internal/tracker"
	"gitlab.com/skyhuborg/tracker/internal/video"
	"gitlab.com/skyhuborg/tracker/pkg/config"
	"gitlab.com/skyhuborg/tracker/pkg/db"
	"gitlab.com/skyhuborg/tracker/pkg/upload"

	"net/http"
	_ "net/http/pprof"
)

type Environment struct {
	Profile                bool
	Debug                  bool
	AutoTLS                bool
	AutoTLSServerName      string
	DbConnectString        string
	DbDriver               string
	DataPath               string
	ConfigFile             string
	TrackerdAddr           string
	UploadAddr             string
	GpsAddr                string
	ControllerTrackerdAddr string
	DevicePathWitmotion    string
	DnnModelPath           string
}

type Tracker struct {
	// Id of Tracker instance
	id string
	// Human readable name of node
	node_name string
	// Command line and Environment variables
	env Environment
	// GRPC Client handle for the trackerd
	client tracker.ClientGRPC
	// GRPC Client handle for the trackerd in the controller
	controllerClient tracker.ClientGRPC
	// GRPC Client configuration
	config tracker.ClientGRPCConfig
	// GRPC Client handle for the trackerd
	uploadClient *upload.Client
	// GRPC Client configuration
	uploadClientConfig upload.ClientConfig
	// SensorMonitor that monitors all sensors
	sensorMonitor sensor.Monitor
	// Configuration settings for the tracker
	settings config.Config
	// database handle
	db db.DB
	// state object for events
	state *event.State
	/* register output channels */
	dbChannel    chan *pb.SensorReport
	videoChannel chan video.VideoRecording
}

var env Environment
var BuildId string
var BuildDate string

func InitializeStore() {
	videoDir := fmt.Sprintf("%s/video", env.DataPath)
	thumbnailDir := fmt.Sprintf("%s/thumbnail", env.DataPath)
	etcDir := fmt.Sprintf("%s/etc", env.DataPath)

	if !common.Mkdir(etcDir) {
		log.Fatalf("Failed making directory: %s\n", etcDir)
	}
	if !common.Mkdir(videoDir) {
		log.Fatalf("Failed making directory: %s\n", etcDir)
	}

	if !common.Mkdir(thumbnailDir) {
		log.Fatalf("Failed making directory: %s\n", etcDir)
	}
}

func DbOutputThread(tracker *Tracker, ch chan *pb.SensorReport) {

	for {
		var (
			rec    db.Sensor
			err    error
			report *pb.SensorReport
			data   []byte
		)

		report = <-ch

		data, err = proto.Marshal(report)

		if err != nil {
			log.Printf("Failed Marshaling SensorReport: %s\n", err)
			continue
		}
		rec.EventId = report.EventId
		rec.Data = data
		tracker.db.AddSensorData(&rec)
	}
}

func DbOutputVideoThread(tracker *Tracker, ch chan video.VideoRecording) {
	client := tracker.client.Client

	for {
		videoRecording := <-ch

		tracker.db.AddVideoEvent(videoRecording.EventId, videoRecording.VideoUri, videoRecording.ThumbnailUri)

		gps_time, _ := ptypes.TimestampProto(time.Now())

		info := &pb.TrackerInfo{
			Uuid:      common.Info.TrackerId,
			Name:      common.Info.TrackerName,
			BuildId:   BuildId,
			BuildDate: BuildDate,
			Hostname:  common.Info.TrackerHostname,
			Time:      gps_time,
			Longitude: common.Info.Longitude,
			Latitude:  common.Info.Latitude,
		}

		videoEvent := &pb.VideoEvent{
			Tracker:   info,
			EventUuid: videoRecording.EventId,
			Uri:       videoRecording.VideoUri,
			Thumbnail: videoRecording.ThumbnailUri,
		}

		_, err := client.AddVideoEvent(context.Background(), videoEvent)

		if err != nil {
			log.Printf("Error: %s\n", err)
		}

	}
}

func VideoUploadThread(t *Tracker) {
	var (
		videoEvent db.VideoEvent
		err        error
	)

	for {
		videoEvent, err = t.db.GetVideoEventNotUploaded()

		if err != nil {
			goto timeout
		}

		videoEvent.IsPending = true
		t.db.Save(videoEvent)

		err = t.uploadClient.Upload(videoEvent.EventId, videoEvent.Uri, videoEvent.Thumbnail)

		if err != nil {
			log.Printf("Error: Upload failed  %s\n", err)
			videoEvent.IsPending = false
			t.db.Save(videoEvent)
			goto timeout
		}
		videoEvent.IsUploaded = true
		t.db.Save(videoEvent)

		continue
	timeout:
		time.Sleep(5 * time.Second)
	}
}

// func ControllerTrackerOutputThread(tracker *Tracker, ch chan *pb.SensorReport) {
// 	client := tracker.controllerClient.Client

// 	for {
// 		report := <-ch
// 		_, err := client.AddSensor(context.Background(), report)

// 		if err != nil {
// 			log.Printf("Error: %s\n", err)
// 		}
// 	}
// }

func TrackerEventOutputThread(tracker *Tracker, ch chan event.Event) {
	client := tracker.client.Client

	for {
		event := <-ch
		ev := event.ToPb()
		_, err := client.AddEvent(context.Background(), ev)

		if err != nil {
			log.Printf("Error: %s\n", err)
		}
	}
}

func TrackerOutputThread(tracker *Tracker, ch chan *pb.SensorReport) {
	client := tracker.client.Client

	for {
		report := <-ch
		_, err := client.AddSensor(context.Background(), report)

		if err != nil {
			log.Printf("Error: %s\n", err)
		}
	}
}

func parseArgs() {
	paramDbConnectString := "db-connect-string"
	paramDbDriver := "db-driver"
	paramDataPath := "data-path"
	paramConfigFile := "config-file"
	paramTrackerdAddr := "trackerd-server-addr"
	paramControllerTrackerdAddr := "controller-trackerd-server-addr"
	paramUploadAddr := "upload-server-addr"
	paramGpsAddr := "gpsd-addr"
	paramDebug := "debug"
	paramProfile := "profile"
	paramDevicePathWitmotion := "device-path-witmotion"
	paramDnnModelPath := "dnn-model-path"
	paramAutoTLS := "auto-tls"
	paramAutoTLSServerName := "auto-tls-server-name"

	envDbConnectString := os.Getenv(paramDbConnectString)
	envDbDriver := os.Getenv(paramDbDriver)
	envDataPath := os.Getenv(paramDataPath)
	envConfigFile := os.Getenv(paramConfigFile)
	envTrackerdAddr := os.Getenv(paramTrackerdAddr)
	envControllerTrackerdAddr := os.Getenv(paramControllerTrackerdAddr)
	envUploadAddr := os.Getenv(paramUploadAddr)
	envGpsAddr := os.Getenv(paramGpsAddr)
	envDebug := os.Getenv(paramDebug)
	envProfile := os.Getenv(paramProfile)
	envDevicePathWitmotion := os.Getenv(paramDevicePathWitmotion)
	envDnnModelPath := os.Getenv(paramDnnModelPath)
	envAutoTLS := os.Getenv(paramAutoTLS)
	envAutoTLSServerName := os.Getenv(paramAutoTLSServerName)

	flag.StringVar(&env.DbConnectString, paramDbConnectString, "skyhub:@tcp(localhost:3306)/skyhub?charset=utf8mb4&parseTime=True&loc=Local", "Connect string for the database")
	flag.StringVar(&env.DbDriver, paramDbDriver, "mysql", "Gorm db driver to use")
	flag.StringVar(&env.DataPath, paramDataPath, "/skyhub/data", "path to data directory")
	flag.StringVar(&env.ConfigFile, paramConfigFile, "/skyhub/etc/tracker.yml", "path to config file")
	flag.StringVar(&env.TrackerdAddr, paramTrackerdAddr, "localhost:8088", "Trackerd server address")
	flag.StringVar(&env.ControllerTrackerdAddr, paramControllerTrackerdAddr, "localhost:8089", "Trackerd server address")
	flag.StringVar(&env.UploadAddr, paramUploadAddr, "http://localhost:8090", "Upload server address")
	flag.StringVar(&env.GpsAddr, paramGpsAddr, "localhost:2947", "GPSD server address")
	flag.BoolVar(&env.Debug, paramDebug, false, "Enable debugging")
	flag.BoolVar(&env.Profile, paramProfile, false, "Enable profiling")
	flag.StringVar(&env.DevicePathWitmotion, paramDevicePathWitmotion, "/dev/hwt901b", "witmotion device path")
	flag.StringVar(&env.DnnModelPath, paramDnnModelPath, "/skyhub/models", "DNN model path")
	flag.BoolVar(&env.AutoTLS, paramAutoTLS, true, "Enable automatic TLS encrypted communication with trackerd using autocert library")
	flag.StringVar(&env.AutoTLSServerName, paramAutoTLSServerName, "trackerd.skyhub.org", "Server name for trackerd")

	flag.Parse()

	if len(envDbConnectString) > 0 {
		env.DbConnectString = envDbConnectString
	}

	if len(envDbDriver) > 0 {
		env.DbDriver = envDbDriver
	}

	if len(envDataPath) > 0 {
		env.DataPath = envDataPath
	}

	if len(envConfigFile) > 0 {
		env.ConfigFile = envConfigFile
	}

	if len(envTrackerdAddr) > 0 {
		env.TrackerdAddr = envTrackerdAddr
	}

	if len(envUploadAddr) > 0 {
		env.UploadAddr = envUploadAddr
	}

	if len(envGpsAddr) > 0 {
		env.GpsAddr = envGpsAddr
	}

	if len(envAutoTLS) > 0 {
		tempAutoTLS, err2 := strconv.ParseBool(envAutoTLS)
		if err2 == nil {
			env.AutoTLS = bool(tempAutoTLS)
		}
	}

	if len(envAutoTLSServerName) > 0 {
		env.AutoTLSServerName = envAutoTLSServerName
	}

	if len(envDebug) > 0 {
		tempDebug, err2 := strconv.ParseBool(envDebug)
		if err2 == nil {
			env.Debug = bool(tempDebug)
		}
	}

	if len(envProfile) > 0 {
		tempProfile, err2 := strconv.ParseBool(envProfile)
		if err2 == nil {
			env.Profile = bool(tempProfile)
		}
	}

	if len(envControllerTrackerdAddr) > 0 {
		env.ControllerTrackerdAddr = envControllerTrackerdAddr
	}

	if len(envDevicePathWitmotion) > 0 {
		env.DevicePathWitmotion = envDevicePathWitmotion
	}

	if len(envDnnModelPath) > 0 {
		env.DnnModelPath = envDnnModelPath
	}
}

func NewTracker(env Environment) (t Tracker, err error) {
	var (
		// handle for the gpsd sensor
		gpsdSensor sensor.GPSDSensor
		// handle for the witmotion hwt901b sensor
		HWT901BSensor sensor.HWT901BSensor
	)

	t.env = env
	err = t.db.Open(env.DbDriver, env.DbConnectString)

	if err != nil {
		return
	}

	// Initialize the subscriber map
	trackerdEventChannel := make(chan event.Event, 10)
	t.state = event.NewState(&t.db)
	t.state.RegisterChannel(trackerdEventChannel)

	// Load the local configuration for the tracker
	err = t.settings.Open(env.ConfigFile)

	if err != nil {
		log.Fatalf("Error: %s\n", err)
		return
	}

	defer t.settings.Close()

	t.id = t.settings.GetUuid()

	if err != nil {
		log.Fatalf("Error: Failed getting Trackerd Id")
		return
	}

	t.node_name = t.settings.GetNodeName()

	if err != nil {
		log.Fatalf("Error: failed getting NodeName")
		return
	}

	// set globals so we can access them anywhere
	common.Info.TrackerId = t.id
	common.Info.TrackerName = t.node_name
	common.Info.TrackerHostname = t.settings.GetHostname()

	// configure and connect to trackerd
	clientConfig := tracker.ClientGRPCConfig{
		Address:           env.TrackerdAddr,
		AutoTLS:           env.AutoTLS,
		AutoTLSServerName: env.AutoTLSServerName}
	t.client, err = tracker.NewClientGRPC(clientConfig)

	// configure and connect to trackerd
	controllerClientConfig := tracker.ClientGRPCConfig{
		Address: env.ControllerTrackerdAddr,
		AutoTLS: false}

	t.controllerClient, err = tracker.NewClientGRPC(controllerClientConfig)

	t.uploadClientConfig = upload.ClientConfig{
		Host:      env.UploadAddr,
		TrackerId: common.Info.TrackerId,
	}

	t.uploadClient = upload.NewClient(&t.uploadClientConfig)

	// create new SensorMonitor attached to the state
	t.sensorMonitor, err = sensor.NewMonitor(t.state)

	if err != nil {
		log.Fatalf("Error: failed creating sensor.Monitor: %s", err)
		return
	}

	// poll sensors every 5 seconds
	t.sensorMonitor.SetPollInterval(time.Second * 5)

	/* register output channels */
	t.dbChannel = make(chan *pb.SensorReport, 10)
	trackerdChannel := make(chan *pb.SensorReport, 10)
	//controllerTrackerdChannel := make(chan *pb.SensorReport, 10)
	t.videoChannel = make(chan video.VideoRecording, 10)

	t.sensorMonitor.RegisterChannel(t.dbChannel)
	t.sensorMonitor.RegisterChannel(trackerdChannel)
	//t.sensorMonitor.RegisterChannel(controllerTrackerdChannel)

	//go ControllerTrackerOutputThread(&t, controllerTrackerdChannel)
	go TrackerOutputThread(&t, trackerdChannel)
	go TrackerEventOutputThread(&t, trackerdEventChannel)
	go DbOutputThread(&t, t.dbChannel)
	go DbOutputVideoThread(&t, t.videoChannel)

	/* register sensors */
	gpsdSensor.Addr = env.GpsAddr
	HWT901BSensor.DevicePath = env.DevicePathWitmotion
	t.sensorMonitor.Register(&gpsdSensor)
	t.sensorMonitor.Register(&HWT901BSensor)

	go t.sensorMonitor.Poll()

	start := time.Now()

	for !common.IsGpsSynchronized() {
		duration := time.Since(start).Seconds()

		if int(duration)%60 == 0 {
			log.Println("Waiting for GPS sync...")
		}

		if duration > 1800 {
			log.Fatalf("Failed to get GPS sync...check your GPS device.")
		}
		time.Sleep(1 * time.Second)
	}

	go t.Start()

	log.Printf("GPS synchronized Time: %s Location: %f,%f \n",
		time.Now(),
		common.Info.Latitude,
		common.Info.Longitude)

	if len(t.settings.GetCameras()) > 0 {
		for _, c := range t.settings.GetCameras() {
			if c.Enabled == false {
				continue
			}

			videoSource := video.VideoSource{
				Uri:      c.Uri,
				Username: c.Username,
				Password: c.Password,
			}

			v := video.NewVideo(env.DataPath)
			v.RegisterVideoChannel(t.videoChannel)

			v.SetState(t.state)

			success := v.Open(&videoSource)

			if !success {
				log.Printf("Failed opening video source: %s\n", videoSource.Uri)
				return
			}

			dnnDetector := video.NewDnn()
			dnnDetector.SetState(t.state)
			dnnDetector.SetModelPath(env.DnnModelPath)

			v.RegisterVideoOutput(dnnDetector)

			go v.Start()
			go t.StartTranscoder(v)
			go v.StartProcessors()
		}
	} else {
		log.Println("No cameras detected.  Operating in Sensor only mode.")
	}
	return
}

func (t *Tracker) Stop() {

	t.client.Close()
	t.controllerClient.Close()
	t.db.Close()
}

func (t *Tracker) Start() (bSuccess bool) {

	log.Printf("Tracker=%s UUID=%s is Online\n", t.node_name, t.id)

	go VideoUploadThread(t)

	return
}

// Transcode one video at a time for the WebUI
// converts mp4 to webm
func (t *Tracker) StartTranscoder(v *video.Video) {
	for _ = range time.Tick(1 * time.Second) {
		var (
			err             error
			videoEvent      db.VideoEvent
			transcodeFailed bool = false
		)

		err = t.db.H.First(&videoEvent, "web_uri='' and transcode_failed=0").Error

		if err != nil {
			continue
		}

		input := videoEvent.Uri
		output := strings.Replace(input, "mkv", "webm", 1)

		if !v.Transcode(input, output) {
			fmt.Printf("Failed transcoding %s to %s\n", input, output)
			transcodeFailed = true
		}

		fmt.Printf("Finished transcoding %s to %s\n", input, output)

		t.db.SetVideoEventWebUri(videoEvent.EventId, output, transcodeFailed)
	}
}

func main() {
	var (
		err        error
		tracker    Tracker
		g_Shutdown bool
	)
	common.Info.BuildId = BuildId
	common.Info.BuildDate = BuildDate

	log.Printf("Running Build ID: %s built at %s\n", common.Info.BuildId, common.Info.BuildDate)
	// load all command line args and env variables
	parseArgs()

	if env.Profile {
		go func() {
			log.Println(http.ListenAndServe(":6060", nil))
		}()
		log.Println("Profiling mode enabled.")
	}

	// creates the directory structure for the data path
	InitializeStore()

	// create our tracker instance and expose the Environment to it
	tracker, err = NewTracker(env)

	if err != nil {
		log.Fatalf("Tracker initialization failed: %s\n", err)
		return
	}

	for _ = range time.Tick(60 * time.Second) {
		if g_Shutdown == true {
			break
		}
	}
	tracker.Stop()
}
